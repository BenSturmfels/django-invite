======
Invite
======

Invite is an event registration app designed to take bookings for public events.
You could use it for any type of event where you just need a name, email address
and number of attendees under that name. It does not handle money.

Detailed documentation is in the "docs" directory.

Requirements
------------

* Django >= 1.3

* a database configured

* Django Admin enabled so you can add events and review registrations.


Quick start
-----------

1. Add Invite to your settings.INSTALLED_APPS::

       INSTALLED_APPS = (
           ...
           'invite',
           ...
       )

2. Include Invite in your project urls.py::

       url(r'^invite/', include('invite.urls')),

3. Run `python manage.py syncdb` to create the Invite models.

4. Start the development server and visit
   http://127.0.0.1:8000/admin/invite/ to add a new event.

5. Visit http://127.0.0.1:8000/invite/[slug]/ to view your event registration
   page.
