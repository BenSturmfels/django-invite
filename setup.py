from setuptools import setup, find_packages

setup(
    name='django-invite',
    version='0.1',
    install_requires=['distribute'],
    packages=find_packages(),
    include_package_data=True,
    long_description=open('README.txt').read(),
    url='https://gitorious.org/django-invite',
    author='Ben Sturmfels',
    author_email='ben@sturm.com.au',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GPL License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
)
