{% autoescape off %}Dear {{ attendee.first_name }},

Thanks for registering {{ attendee.heads }} {{ attendee.heads|pluralize:'person,people' }} for the following event:

{{ event.title }}

{{ event.time|date:"j N, Y, P" }}

{{ event.location|striptags }}

{{ event.followup_instructions }}


Regards,

[Your name]
{% endautoescape %}
