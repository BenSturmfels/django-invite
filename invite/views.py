import logging

from django.conf import settings
from django.core.mail import send_mail
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.template.loader import render_to_string
from django.utils.text import wrap

from invite.models import Event
from invite.forms import AttendeeForm

logger = logging.getLogger(__name__)

def show_event(request, slug):
    """Show details for and accept registration for an event."""

    event = get_object_or_404(Event, slug=slug)

    if request.method == 'POST':
        # TODO: I wonder if it should be "registration has closed" rather than
        # "passed". It might be more useful for registrations to automatically
        # close at a designated time.
        if event.has_passed():
            return HttpResponse("Sorry, the event has now passed.")
        form = AttendeeForm(request.POST)
        if form.is_valid():
            attendee = form.save(commit=False)
            attendee.event = event
            attendee.save()
            body = wrap(render_to_string('invite/confirmation_email.txt',
                                         {'attendee': attendee,
                                          'event': event}), 70)
            send_mail('Registered for: %s, %s'
                      % (event.title,
                         event.time.strftime('%d/%m/%Y')),
                      body, settings.DEFAULT_FROM_EMAIL, [attendee.email])
            logger.info("%s registered for %s" % (attendee, event))
            return redirect('invite.views.thanks')
    else:
        form = AttendeeForm()

    return render(request, 'invite/show_event.html',
                  {'event': event,
                   'form': form})


def thanks(request):
    """Confirm successful registration."""

    return render(request, 'invite/thanks.html')
