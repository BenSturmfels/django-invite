from django import forms

from invite.models import Attendee

class AttendeeForm(forms.ModelForm):
    error_css_class = 'error'

    class Meta:
        model = Attendee
        fields = ['name', 'email', 'heads', 'follow_up']
