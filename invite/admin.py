from django.contrib import admin
from invite.models import Attendee, Event

class EventAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}

class AttendeeAdmin(admin.ModelAdmin):
    list_display = ('name', 'event', 'heads', 'time')
    list_filter = ('event',)

admin.site.register(Attendee, AttendeeAdmin)
admin.site.register(Event, EventAdmin)
