from django.conf.urls import patterns

urlpatterns = patterns('invite.views',
    (r'^thanks/$', 'thanks'),
    (r'^(?P<slug>.+)/$', 'show_event'),
)
