import datetime

from django.db import models

class Event(models.Model):
    """The event people are invited to attend."""

    title = models.CharField(max_length=255)
    subtitle = models.CharField(max_length=255, blank=True)
    slug = models.SlugField()
    time = models.DateTimeField()
    location = models.TextField()
    description = models.TextField()
    followup_instructions = models.TextField(blank=True)

    def __unicode__(self):
        return self.title

    def has_passed(self):
        if datetime.date.today() > self.time.date():
            return True
        else:
            return False


class Attendee(models.Model):
    """An RSVP to an event."""

    event = models.ForeignKey(Event, editable=False)
    name = models.CharField(max_length=255)
    email = models.EmailField()
    heads = models.IntegerField('Number of people', default=1)
    follow_up = models.BooleanField('Inform me of similar events', default=True)
    time = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return "%s <%s>" % (self.name, self.email)

    def first_name(self):
        return self.name.split()[0]
